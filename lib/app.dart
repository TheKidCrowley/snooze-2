///
/// Created by Jonatan on 06.05.2020.
///
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:Snooze/bloc/langChanger.dart';
import 'package:Snooze/locale/app_localization.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:Snooze/bloc/themeChanger.dart';
import 'screens/terminal.dart';
import 'screens/drawer/submenus/import_all.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'persistentStorage/userdata.dart' as userdata;

class App extends StatelessWidget {

  String get _title => 'Snooze';

  final AppLocalizationDelegate _localeOverrideDelegate = AppLocalizationDelegate(Locale('en', 'US'));

  @override
  Widget build(BuildContext context) {
    // set to fullscreen
    SystemChrome.setEnabledSystemUIOverlays([]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeChanger>(
          create: (_) => ThemeChanger(userdata.themeLight)),
        ChangeNotifierProvider<LanguageChanger>(
          create: (_) => LanguageChanger(_localeOverrideDelegate.overriddenLocale.languageCode.toString())),],
      child: MaterialAppWithProvider(title: _title, localeOverrideDelegate: _localeOverrideDelegate),
    );
  }
}

class MaterialAppWithProvider extends StatelessWidget {
  const MaterialAppWithProvider({
    Key key,
    @required String title,
    @required AppLocalizationDelegate localeOverrideDelegate,
  }) : _title = title, _localeOverrideDelegate = localeOverrideDelegate, super(key: key);

  final String _title;
  final AppLocalizationDelegate _localeOverrideDelegate;
  final List<Locale> _locales = const [Locale('en', 'US'), Locale('de', 'DE'), Locale('es', 'ES')];

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeChanger>(context);
    final lang = Provider.of<LanguageChanger>(context);
    switch(lang.getLanguage){
      case userdata.languageEN: AppLocalization.load(_locales[0]);
                                break;
      case userdata.languageDE: AppLocalization.load(_locales[1]);
                                break;
      case userdata.languageES: AppLocalization.load(_locales[2]);
                                break;
    }
    if(lang.getRecentLanguage==null || lang.getRecentLanguage!=lang.getLanguage){
      Timer(new Duration(milliseconds: 50),  () {
        lang.setLanguage(lang.getLanguage);
        lang.updateRecentLanguage();
      });
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      theme: theme.getTheme(),
      home: Terminal(title: this._title),
      routes: {
        userdata.routeSettings: (context) =>
            new SubMenuContainer(AppLocalization.of(context).settings, new SettingsPage()),
        userdata.routeSettingsLang: (context) =>
            new SubMenuContainer(AppLocalization.of(context).language, new SettingsLangPage()),
        userdata.routeSettingsTheme: (context) =>
            new SubMenuContainer(AppLocalization.of(context).theme, new SettingsThemePage()),
        userdata.routeAccountDetails: (context) =>
            new SubMenuContainer(AppLocalization.of(context).accountDetails, new AccountDetails()),
        userdata.routeStartTutorial: (context) => new StartTutorial(),
        userdata.routeSupport: (context) => new SubMenuContainer(AppLocalization.of(context).support, new Support()),
      },
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        _localeOverrideDelegate
      ],
      supportedLocales: _locales,
    );
  }
}
