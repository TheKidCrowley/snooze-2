///
/// Created by Jonatan on 10.06.2020.
///

import 'package:flutter/material.dart';
import 'package:Snooze/persistentStorage/userdata.dart' as userdata;

class ThemeChanger with ChangeNotifier {

  ThemeData _themeData;
  String _themeName;

  static const Color snoozeColor = Color.fromRGBO(56, 199, 239, 1.0);

  Map<String, ThemeData> _themes = {
    userdata.themeLight:  ThemeData(
                            brightness: Brightness.light,
                            primaryColor: snoozeColor,
                            visualDensity: VisualDensity.adaptivePlatformDensity,
                            primaryColorBrightness: Brightness.light,
                            bottomAppBarColor: snoozeColor,
                            accentColor: Colors.amber,
                            accentColorBrightness: Brightness.light,
                            unselectedWidgetColor: Colors.black,
                            buttonColor: snoozeColor,
                          ),
    userdata.themeDark:   ThemeData(
                            brightness: Brightness.dark,
                            primaryColor: snoozeColor,
                            visualDensity: VisualDensity.adaptivePlatformDensity,
                            primaryColorBrightness: Brightness.dark,
                            bottomAppBarColor: snoozeColor,
                            accentColor: Colors.amber,
                            accentColorBrightness: Brightness.dark,
                            unselectedWidgetColor: Colors.white,
                            buttonColor: snoozeColor,
                          ),
    // 'snooze': ThemeData(
    //             brightness: Brightness.light,
    //             primaryColor: snoozeColor,
    //             visualDensity: VisualDensity.adaptivePlatformDensity,
    //           ),
  };

  ThemeChanger(this._themeName){
    this.setTheme(this._themeName);
  }

  getTheme() => _themeData;

  String get getThemeName => this._themeName;

  setTheme(String themeName){
    _themeName = themeName;
    _themeData = _themes[themeName];

    notifyListeners();
  }
}