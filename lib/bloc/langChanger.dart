///
/// Created by Jonatan on 10.06.2020.
///

import 'package:flutter/material.dart';

class LanguageChanger with ChangeNotifier {

  String _language;
  String _recentLanguage;

  LanguageChanger(this._language);

  String get getLanguage => _language;

  setLanguage(String language){
    _language = language;
    
    notifyListeners();
  }

  String get getRecentLanguage => _recentLanguage;

  updateRecentLanguage() => this._recentLanguage=_language;
}