// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'messages';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "accountDetails" : MessageLookupByLibrary.simpleMessage("Account"),
    "add" : MessageLookupByLibrary.simpleMessage("Add"),
    "addFavorite" : MessageLookupByLibrary.simpleMessage("Add position as favorite"),
    "aura" : MessageLookupByLibrary.simpleMessage("Aura"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "continueButton" : MessageLookupByLibrary.simpleMessage("Continue"),
    "dark" : MessageLookupByLibrary.simpleMessage("Dark"),
    "deletedFavorite" : MessageLookupByLibrary.simpleMessage("Deleted Favorite"),
    "heyWorld" : MessageLookupByLibrary.simpleMessage("Hey World"),
    "home" : MessageLookupByLibrary.simpleMessage("Home"),
    "language" : MessageLookupByLibrary.simpleMessage("Language"),
    "leave" : MessageLookupByLibrary.simpleMessage("Leave now"),
    "leaveConfirm" : MessageLookupByLibrary.simpleMessage("Do you want to leave now?"),
    "light" : MessageLookupByLibrary.simpleMessage("Light"),
    "music" : MessageLookupByLibrary.simpleMessage("Music"),
    "name" : MessageLookupByLibrary.simpleMessage("Name"),
    "ok" : MessageLookupByLibrary.simpleMessage("OK"),
    "seat" : MessageLookupByLibrary.simpleMessage("Seat"),
    "sessionEnd" : MessageLookupByLibrary.simpleMessage("Your session is about to end"),
    "settings" : MessageLookupByLibrary.simpleMessage("Settings"),
    "snooze" : MessageLookupByLibrary.simpleMessage("Snooze"),
    "startSnoozing" : MessageLookupByLibrary.simpleMessage("Snooze"),
    "startTutorial" : MessageLookupByLibrary.simpleMessage("Start tutorial"),
    "storeUpToFavorites" : MessageLookupByLibrary.simpleMessage("You can only store up to 6 favorites"),
    "support" : MessageLookupByLibrary.simpleMessage("Support"),
    "thankYou" : MessageLookupByLibrary.simpleMessage("Thank you for choosing us today"),
    "theme" : MessageLookupByLibrary.simpleMessage("Theme"),
    "tutorial" : MessageLookupByLibrary.simpleMessage("Tutorial"),
    "undo" : MessageLookupByLibrary.simpleMessage("Undo"),
    "yes" : MessageLookupByLibrary.simpleMessage("Yes")
  };
}
