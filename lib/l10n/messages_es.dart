// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "accountDetails" : MessageLookupByLibrary.simpleMessage("Cuenta"),
    "add" : MessageLookupByLibrary.simpleMessage("Añadir"),
    "addFavorite" : MessageLookupByLibrary.simpleMessage("Agregar posición como favorito"),
    "aura" : MessageLookupByLibrary.simpleMessage("Ligero"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancelar"),
    "continueButton" : MessageLookupByLibrary.simpleMessage("Siguiente"),
    "dark" : MessageLookupByLibrary.simpleMessage("Oscuro"),
    "deletedFavorite" : MessageLookupByLibrary.simpleMessage("Favorito eliminado"),
    "heyWorld" : MessageLookupByLibrary.simpleMessage("Hola Mundo"),
    "home" : MessageLookupByLibrary.simpleMessage("Casa"),
    "language" : MessageLookupByLibrary.simpleMessage("Idioma"),
    "leave" : MessageLookupByLibrary.simpleMessage("Salir inmediato"),
    "leaveConfirm" : MessageLookupByLibrary.simpleMessage("¿Quieres irte ahora?"),
    "light" : MessageLookupByLibrary.simpleMessage("Iluminado"),
    "music" : MessageLookupByLibrary.simpleMessage("Musica"),
    "name" : MessageLookupByLibrary.simpleMessage("Nombre"),
    "ok" : MessageLookupByLibrary.simpleMessage("OK"),
    "seat" : MessageLookupByLibrary.simpleMessage("Asiento"),
    "sessionEnd" : MessageLookupByLibrary.simpleMessage("Tu sesión está por terminar"),
    "settings" : MessageLookupByLibrary.simpleMessage("Ajustes"),
    "snooze" : MessageLookupByLibrary.simpleMessage("Snooze"),
    "startSnoozing" : MessageLookupByLibrary.simpleMessage("Snooze"),
    "startTutorial" : MessageLookupByLibrary.simpleMessage("Comienzo tutorial"),
    "storeUpToFavorites" : MessageLookupByLibrary.simpleMessage("Solo puedes almacenar hasta 6 favoritas"),
    "support" : MessageLookupByLibrary.simpleMessage("Support"),
    "thankYou" : MessageLookupByLibrary.simpleMessage("Gracias por elegirnos hoy"),
    "theme" : MessageLookupByLibrary.simpleMessage("Tema"),
    "tutorial" : MessageLookupByLibrary.simpleMessage("Tutorial"),
    "undo" : MessageLookupByLibrary.simpleMessage("Deshacer"),
    "yes" : MessageLookupByLibrary.simpleMessage("Si")
  };
}
