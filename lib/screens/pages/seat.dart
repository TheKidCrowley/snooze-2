///
/// Created by Jonatan on 09.05.2020.
///
import 'dart:async';
import 'package:Snooze/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:Snooze/persistentStorage/sharedPref.dart';
import 'package:Snooze/persistentStorage/Favorite.dart';
import 'package:Snooze/persistentStorage/userdata.dart' as userdata;

class Seat extends StatefulWidget {
  @override
  _SeatState createState() => _SeatState();
}

class _SeatState extends State<Seat> {

  final String _degree = '°';

  double _minRange = 0.0;
  double _backrestMaxRange = 88.0;
  double _middleMaxRange = 30.0;
  double _feetrestMaxRange = 90.0;

  double _backrestValue = 88.0;
  double _middleValue = 0.0;
  double _feetrestValue = 5.0;

  bool _isDraggingBackrest = false;
  bool _isDraggingMiddlerest = false;
  bool _isDraggingFeetrest = false;

  double _spacingBetweenSliders = 50.0;

  double _inputSize = 50.0;

  TextStyle _textstyle = new TextStyle(fontSize: 20.0);

  int _indexSelectedChoiceChip = -1;
  bool _isPositionStoredFavorite = false;
  bool _undoActive = false;
  bool _fadeOutUndo = false;
  bool _tooManyFavorites = false;
  bool _fadeOutTooManyFavorites = false;
  Favorite _recentlyRemovedFavorite;
  final Duration _fadeoutDuration = Duration(milliseconds: 500);
  List <Favorite> _favorites = new List <Favorite>();
  Timer _undoTimer;
  Timer _undoTimerintern;
  Timer _tooManyFavoritesTimer;
  Timer _undoFadeOutTimer;
  Timer _tooManyFavoritesFadeOutTimer;

  SharedPref sharedPref = new SharedPref();
  List<String> favObjectNames = [userdata.favoriteOne,
                                  userdata.favoriteTwo,
                                  userdata.favoriteThree,
                                  userdata.favoriteFour,
                                  userdata.favoriteFive,
                                  userdata.favoriteSix];


  @override
  initState(){
    super.initState();
    _loadSharedPreferences();
  }

  _loadSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //load angle values
    _backrestValue = (prefs.getDouble(userdata.backRestValue) ?? 0.0);
    _middleValue = (prefs.getDouble(userdata.middleRestValue) ?? 0.0);
    _feetrestValue = (prefs.getDouble(userdata.feetRestValue) ?? 0.0);
    //load favorites
    for(String favObjName in favObjectNames){
      dynamic result = await sharedPref.read(favObjName);
      if(result!=null){
        Favorite fav = Favorite.fromJson(result);
        if(fav!=null && fav.name!=null && fav.name.isNotEmpty){
          _favorites.add(fav);
        }
      }
    }
    _rebuild();
  }

  @override
  dispose(){
    super.dispose();
    _undoTimer?.cancel();
    _undoTimerintern?.cancel();
    _tooManyFavoritesTimer?.cancel();
    _undoFadeOutTimer?.cancel();
    _tooManyFavoritesFadeOutTimer?.cancel();
    _writeToSharedPreferences();
  }

  _writeToSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    //store angle values
    prefs.setDouble(userdata.backRestValue, _backrestValue);
    prefs.setDouble(userdata.middleRestValue, _middleValue);
    prefs.setDouble(userdata.feetRestValue, _feetrestValue);

    //remove the deleted favorites
    for(int i=_favorites.length;i<favObjectNames.length;i++){
      sharedPref.remove(favObjectNames[i]);
    }
    //store favorites
    for(int i=0;i<_favorites.length;i++){
      Favorite fav = _favorites[i];
      if(fav!=null && fav.name!=null && fav.name.isNotEmpty){
        sharedPref.save(favObjectNames[i], fav);
      }
    }
  }

  _rebuild() => setState((){});

  setRestPosition(double backrest, double middle, double feetrest){
    this._backrestValue = backrest;
    this._middleValue = middle;
    this._feetrestValue = feetrest;
  }


//favorites handler
  _checkChoiceChip(){
    for(Favorite favorite in _favorites){
      if(_compareWODec(_backrestValue, favorite.back) && _compareWODec(_middleValue, favorite.middle) && _compareWODec(_feetrestValue, favorite.feet)){
        _indexSelectedChoiceChip = _favorites.indexOf(favorite);
        break;
      }
      _indexSelectedChoiceChip = -1;
    }
    _isPositionStoredFavorite = _favorites.isNotEmpty && _indexSelectedChoiceChip!=-1 ? true : false;
  }
  double cleanDouble(double input) => double.parse(input.toStringAsFixed(0));
  bool _compareWODec(double x, double y) => cleanDouble(x).toInt()==y.toInt();
  List <Widget> generateChoiceChipsFromFavorites(){
    List<Widget> _temp = new List<Widget>();
    for(int i=0;i<_favorites.length;i++){
      if(i>0){
        _temp.add(
          const SizedBox(width: 20.0,),
        );
      }
      _temp.add(
        new ChoiceChip(
          label: Text(_favorites[i].name),
          selected: _indexSelectedChoiceChip == i,
          onSelected: (value) {
            setState(() {
              _indexSelectedChoiceChip = value ? i : -1;
              setRestPosition(_favorites[i].back, _favorites[i].middle, _favorites[i].feet);
            });
          },
        ),
      );
    }
    return _temp;
  }
  //add favorite dialog
  Future<String> addFavorite(BuildContext context){

    TextEditingController _controller = TextEditingController();

    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text(AppLocalization.of(context).addFavorite),
        content: new Container(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Text(
                AppLocalization.of(context).name+': ',
              ),
              new Container(
                width: 170.0,
                child: new TextField(
                  controller: _controller,
                  maxLength: 20,
                  autofocus: true,
                ),
              ),
            ],
          ),
        ),
        actions: <Widget> [
          OutlineButton(
            child: Text(AppLocalization.of(context).cancel),
            onPressed: () => Navigator.of(context).pop(),
          ),
          OutlineButton(
            child: Text(AppLocalization.of(context).add),
            onPressed: () => _controller.text.toString().isNotEmpty 
                  ? Navigator.of(context).pop(_controller.text.toString())
                  : print('seat.addFavorite() Error: Cannot add favorite with empty name!'),
          )
        ],
      );
    });
  }
  //called when star.onPressed
  _manageFavorite(){
    if(!_isPositionStoredFavorite){
      if(_favorites.length>5){
        _displayTooManyFavorites();
        return;
      }
      addFavorite(context).then((onValue) {
        if(onValue!=null && onValue.isNotEmpty){
          _favorites.add(new Favorite(onValue, cleanDouble(_backrestValue), cleanDouble(_middleValue), cleanDouble(_feetrestValue)));
          _rebuild();
        }
      });
    }
    else{
      for(Favorite fav in _favorites){
        if(_compareWODec(_backrestValue, fav.back) && _compareWODec(_middleValue, fav.middle) && _compareWODec(_feetrestValue, fav.feet)){
          _recentlyRemovedFavorite = fav;
          _favorites.remove(fav);
          _showUndoButton();
          break;
        }
      }
      _rebuild();
    }
  }
  //inform user about too many stored favorites
  _displayTooManyFavorites(){
    _tooManyFavoritesTimer?.cancel();
    print('seat._displayTooManyFavorites() Error: too many favorites, you can only store 6 favorites');
    _tooManyFavorites = true;
    _rebuild();
    _tooManyFavoritesTimer = Timer(Duration(seconds: 2), (){
      _fadeOutTooManyFavorites = true;
      _rebuild();
      _tooManyFavoritesFadeOutTimer = Timer(_fadeoutDuration, () {
        _tooManyFavorites = false;
        _fadeOutTooManyFavorites = false;
        _rebuild();
      });
    });
  }
  //option to undo removal of favorite
  _showUndoButton(){
    _undoActive = false;
    _fadeOutUndo = false;
    _undoTimer?.cancel();
    _undoTimerintern?.cancel();
    _rebuild();
    _undoActive = true;
    _rebuild();
    _undoTimer = Timer(Duration(seconds: 3), (){
      _undoTimerintern?.cancel();
      _fadeOutUndo = true;
      _rebuild();
      _undoFadeOutTimer = Timer(_fadeoutDuration, () {
        _undoActive = false;
        _fadeOutUndo = false;
        _rebuild();
      });
    });
  }

//api connection
  _notifyAPI(){
    int bedLegAngle = _feetrestValue.toInt();
    int bedBackAngle = _backrestValue.toInt();
    int bedMidAngle = _middleValue.toInt();

    //TODO: #api post request mit aktuellen werten (s.o.)


  }



  @override
  Widget build(BuildContext context) {

    //TODO: limit, dass nur jede sekunde ein call gemacht wird
    _notifyAPI();

    _checkChoiceChip();
    return Container(
      child: new Column(
        children: <Widget> [
          new SizedBox(
            height: 10.0,
          ),
          new Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              //animation of lounger
              new Expanded(
                child: new Center(
                  child: new Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      CustomPaint(
                        size: Size(100, 300),
                        painter: RestPainter(
                          radius: 100.0, 
                          radians: -math.pi+math.pi*.5/90.0*_backrestValue, 
                          angleOffset: 'right',
                          startradians: 0.0,
                          isDragging: _isDraggingBackrest, 
                          angleIndicator: false,
                          context: context,
                        ),
                      ),
                      new Stack(
                        alignment: Alignment.centerLeft,
                        children: <Widget> [
                          CustomPaint(
                            size: Size(100, 300),
                            painter: RestPainter(
                              radius: 100.0, 
                              radians: -math.pi*.5/90.0*_middleValue,
                              angleOffset: 'left',
                              isDragging: _isDraggingMiddlerest,
                              angleIndicator: true,
                              context: context,
                            ),
                          ),
                          CustomPaint(
                            size: Size(100, 300),
                            painter: RestPainter(
                              radius: 100.0, 
                              angleOffset: 'custom',
                              radians: math.pi*.5-math.pi*.5/90.0*(_feetrestValue+_middleValue), 
                              startradians: -math.pi*.5/90.0*_middleValue, 
                              isDragging: _isDraggingFeetrest, 
                              angleIndicator: true,
                              context: context,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: 100,),
                    ],
                  ),
                ),
              ),
              //slidecontrols for lounger
              new Expanded(
                child: new Column(
                  children: [
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        //favourite star
                        new IconButton(
                          onPressed: _manageFavorite,
                          icon: _isPositionStoredFavorite 
                            ? Icon(Icons.star, color: Colors.yellowAccent[700],) 
                            : Icon(Icons.star_border,),
                        ),
                        new SizedBox(
                          width: 10.0,
                        ),
                      ],
                    ),
                    //BACKREST
                    new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Semantics(
                          child: SizedBox(
                            width: _inputSize,
                            height: _inputSize,
                            child: TextField(
                              textAlign: TextAlign.end,
                              style: _textstyle,
                              onSubmitted: (value) {
                                final newValue = double.tryParse(value);
                                if (newValue != null && newValue != _backrestValue) {
                                  setState(() {
                                    _backrestValue = newValue.clamp(_minRange, _backrestMaxRange) as double;
                                  });
                                }
                              },
                              keyboardType: TextInputType.number,
                              controller: TextEditingController(
                                text: cleanDouble(_backrestValue).truncate().toString(),
                              ),
                            ),
                          ),
                        ),
                        new Text(
                          _degree,
                          style: _textstyle,
                        ),
                        new Expanded(
                          child: new Slider(
                            value: _backrestValue,
                            min: _minRange,
                            max: _backrestMaxRange,
                            onChanged: (value) => setState(() => _backrestValue = value),
                            onChangeStart: (value) => setState(() => _isDraggingBackrest=true),
                            onChangeEnd: (value) => setState(() => _isDraggingBackrest=false),
                          ),
                        ),
                      ],
                    ),
                    new SizedBox(
                      height: _spacingBetweenSliders,
                    ),
                    //MIDDLEREST
                    new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Semantics(
                          child: SizedBox(
                            width: _inputSize,
                            height: _inputSize,
                            child: TextField(
                              textAlign: TextAlign.end,
                              style: _textstyle,
                              onSubmitted: (value) {
                                final newValue = double.tryParse(value);
                                if (newValue != null && newValue != _middleValue) {
                                  setState(() {
                                    _middleValue = newValue.clamp(_minRange, _middleMaxRange) as double;
                                  });
                                }
                              },
                              keyboardType: TextInputType.number,
                              controller: TextEditingController(
                                text: cleanDouble(_middleValue).truncate().toString(),
                              ),
                            ),
                          ),
                        ),
                        new Text(
                          _degree,
                          style: _textstyle,
                        ),
                        new Expanded(
                          child: new Slider(
                            value: _middleValue,
                            min: _minRange,
                            max: _middleMaxRange,
                            onChanged: (value) => setState(() => _middleValue = value),
                            onChangeStart: (value) => setState(() => _isDraggingMiddlerest=true),
                            onChangeEnd: (value) => setState(() => _isDraggingMiddlerest=false),
                          ),
                        ),
                      ],
                    ),
                    new SizedBox(
                      height: _spacingBetweenSliders,
                    ),
                    //FEETREST
                    new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Semantics(
                          child: SizedBox(
                            width: _inputSize,
                            height: _inputSize,
                            child: TextField(
                              textAlign: TextAlign.end,
                              style: _textstyle,
                              onSubmitted: (value) {
                                final newValue = double.tryParse(value);
                                if (newValue != null && newValue != _feetrestValue) {
                                  setState(() {
                                    _feetrestValue = newValue.clamp(_minRange, _feetrestMaxRange) as double;
                                  });
                                }
                              },
                              keyboardType: TextInputType.number,
                              controller: TextEditingController(
                                text: cleanDouble(_feetrestValue).truncate().toString(),
                              ),
                            ),
                          ),
                        ),
                        new Text(
                          _degree,
                          style: _textstyle,
                        ),
                        new Expanded(
                          child: new Slider(
                            value: _feetrestValue,
                            min: _minRange,
                            max: _feetrestMaxRange,
                            onChanged: (value) => setState(() => _feetrestValue = value),
                            onChangeStart: (value) => setState(() => _isDraggingFeetrest=true),
                            onChangeEnd: (value) => setState(() => _isDraggingFeetrest=false),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 30.0),
          //ChoiceChips
          new Expanded(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: generateChoiceChipsFromFavorites(),
            )
          ),
          //display UNDO button
          _undoActive 
            ? new AnimatedOpacity(
                opacity: _fadeOutUndo ? 0.0 : 1.0, 
                duration: _fadeoutDuration,
                child: new Row(
                  children: [
                    const SizedBox(width: 20.0,),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[700],
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: new Row(
                        children: [
                          const SizedBox(width: 5.0,),
                          Text(AppLocalization.of(context).deletedFavorite, style: new TextStyle(color: Colors.grey[100]),),
                          const SizedBox(width: 10.0,),
                          new RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            color: Colors.grey[500],
                            child: new Text(AppLocalization.of(context).undo, style: new TextStyle(fontSize: 20.0)),
                            onPressed: () => setState(() {
                              _favorites.add(_recentlyRemovedFavorite);
                              _undoActive = false;
                              _fadeOutUndo = false;
                            }),
                          ),
                          const SizedBox(width: 5.0,),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            : new Container(),
          _tooManyFavorites
            ? new AnimatedOpacity(
                opacity: _fadeOutTooManyFavorites ? 0.0 : 1.0,
                duration: _fadeoutDuration,
                child: new Row(
                  children: [
                    const SizedBox(width: 20.0,),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
                      decoration: BoxDecoration(
                        color: Colors.grey[700],
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Text(
                        AppLocalization.of(context).storeUpToFavorites, 
                        style: new TextStyle(color: Colors.grey[100], fontSize: 20.0,),
                      ),
                    ),
                  ],
                ),
              )
            : new Container(),
          const SizedBox(height: 15.0,),
        ],
      ),
    );
  }
}

class RestPainter extends CustomPainter {
  const RestPainter({
    Key key,
    this.radius, 
    this.radians, 
    this.startradians, 
    this.isDragging, 
    this.angleIndicator,
    this.angleOffset,
    this.context,
  });
  final double radius;
  final double radians;
  final double startradians;
  final bool isDragging;
  final bool angleIndicator;
  final String angleOffset;
  final BuildContext context;

  @override
  void paint(Canvas canvas, Size size) {
    var _linePaint = Paint()
      ..color = isDragging? Colors.red : Theme.of(context).primaryColor
      ..strokeWidth = 12.0
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    var _pointPaint = Paint()
      ..color = Colors.black;

    var _pointDecorationPaint = Paint()
      ..color = Colors.white70;

    var path = Path();

    double dx, dy;

    switch(angleOffset){
      case 'right':   dx = size.width;
                      dy = size.height / 2;
                      break;
      case 'left':    dx = 0.0;
                      dy = size.height / 2;
                      break;
      case 'custom':  dx = radius * math.cos(startradians);
                      dy = radius * math.sin(startradians) + size.height/2;
                      break;
    }

    Offset angle = Offset(
      dx,
      dy,
    );

    path.moveTo(angle.dx, angle.dy);

    Offset pointOnCircle = Offset(
      radius * math.cos(radians) + angle.dx,
      radius * math.sin(radians) + angle.dy,
    );

    path.lineTo(pointOnCircle.dx, pointOnCircle.dy);

    canvas.drawPath(path, _linePaint);

    if(angleIndicator){
      canvas.drawCircle(angle, _linePaint.strokeWidth/2, _pointDecorationPaint);
      canvas.drawCircle(angle, _linePaint.strokeWidth/2-2.0, _pointPaint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
