/// @Autor Cyrilluis

import 'package:Snooze/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hsvcolor_picker/flutter_hsvcolor_picker.dart';
import 'package:simple_color_picker/simple_color_picker.dart';

class Aura extends StatefulWidget {
  @override
  _AuraState createState() => _AuraState();
}

class _AuraState extends State<Aura> {
  HSVColor color = HSVColor.fromColor(Colors.cyan);

  void onChanged(HSVColor value) => this.color = value;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: new Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, top: 10),
                    child: Text(
                      'Lichtfarbe',
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'SourceSerifPro',
                        fontSize: 30,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 2.0,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(left: 80, top: 10),
                  child: Text(
                    'wähle deine Wunschfarbe',
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'SourceSerifPro',
                      fontSize: 25,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 2.0,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
            ],
          ),
          new Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child: SafeArea(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Card(
                        color: Colors.transparent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(0.0))),
                        elevation: 0,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              FloatingActionButton(
                                onPressed: () {},
                                backgroundColor: this.color.toColor(),
                              ),
                              Divider(),
                              SimpleColorPicker(
                                color: this.color,
                                onChanged: (value) => super.setState(() => this.onChanged(value)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              /*Expanded(
                flex: 1,
                child: localAsset(),
              ),*/
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(0.0),
                child: localAsset(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

Widget _btn(
  String txt,
  /*VoidCallback onPressed*/
) {
  return ButtonTheme(
    minWidth: 48.0,
    child: Container(
      width: 150,
      height: 50,
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(35)),
        child: Text(
          txt,
          style: TextStyle(
            fontFamily: 'SourceSerifPro',
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
        color: Colors.pink[900],
        textColor: Colors.white,
        onPressed: () {},
      ),
    ),
  );
}

Widget localAsset() {
  return Container(
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(0.0),
              child: _btn('Nachtlicht'),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8, top: 0),
              child: _btn('Sommer'),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8, top: 0),
              child: _btn('Wald'),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8, top: 0),
              child: _btn('Herbst'),
            ),
          ],
        ),
        //  Padding(padding: EdgeInsets.all(10), child: slider())
      ],
    ),
  );
}
