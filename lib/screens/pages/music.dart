/// @Autor Cyrilluis

import 'package:Snooze/locale/app_localization.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class Music extends StatefulWidget {
  @override
  _MusicState createState() => _MusicState();
}

class _MusicState extends State<Music> {
  AudioPlayer _audioPlayer = AudioPlayer();
  bool isPlaying = false;
  String currentTime = "00:00";
  String completeTime = "00:00";

  @override
  void initState(){
    super.initState();
  }
  
/*
  Duration _duration = Duration();
  Duration _position = Duration();
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
*/
/*
  @override
  void initState() {
    super.initState();
    initPlayer();
  }
*/
/*
  void initPlayer() {
    advancedPlayer = AudioPlayer();
    audioCache = AudioCache(fixedPlayer: advancedPlayer);

    advancedPlayer.durationHandler = (d) {
      setState(() {
        _duration = d;
      });
    };

    advancedPlayer.positionHandler = (p) {
      setState(() {
        _position = p;
      });
    };
  }
*/
 /* String localFilePath;*/
/*
  Widget _btn(String txt, VoidCallback onPressed) {
    return ButtonTheme(
      minWidth: 48.0,
      child: Container(
        width: 100,
        height: 70,
        child: RaisedButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Text(
              txt,
              style: TextStyle(
                fontFamily: 'SourceSerifPro',
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
            color: Colors.pink[900],
            textColor: Colors.white,
            onPressed: onPressed),
      ),
    );
  }
*/
/*
  Widget slider() {
    return Slider(
        activeColor: Colors.black,
        inactiveColor: Colors.pink,
        value: _position.inSeconds.toDouble(),
        min: 0.0,
        max: _duration.inSeconds.toDouble(),
        onChanged: (double value) {
          setState(() {
            seekToSecond(value.toInt());
            value = value;
          });
        });
  }
*/
  Widget localAsset() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only( top: 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    color: Colors.pink[900],
                    icon: Icon(
                      Icons.play_arrow,
                      size: 90,
                    ),
                    onPressed: () {
                      print("");
                    },
                  ), //_btn('Play', () => audioCache.play('music/drivebymusik.mp3')),
                ),
                SizedBox(
                  width: 30,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    color: Colors.pink[900],
                    icon: Icon(
                      Icons.pause,
                      size: 90,
                    ),
                    onPressed: () {
                      print("");
                    },
                  ),
                  //_btn('Pause', () => advancedPlayer.pause()),
                ),
                SizedBox(
                  width: 30,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    color: Colors.pink[900],
                    icon: Icon(
                      Icons.stop,
                      size: 90,
                    ),
                    onPressed: () {
                      print("");
                    },
                  ),
                  //_btn('Stop', () => advancedPlayer.stop()),
                ),
              ],
            ),
            //  Padding(padding: EdgeInsets.all(10), child: slider())
          ],
        ),
      ),
    );
  }
  /*
  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);

    advancedPlayer.seek(newDuration);
  }
*/
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: new Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 10),
                        child: Text(
                          'Musik',
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'SourceSerifPro',
                            fontSize: 30,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 2.0,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 80, top: 10),
                          child: Text(
                            'Wähle deine Musikwunsch',
                            style: TextStyle(
                              color: Colors.grey,
                              fontFamily: 'SourceSerifPro',
                              fontSize: 25,
                              fontWeight: FontWeight.w700,
                              letterSpacing: 2.0,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.0),
                  Padding(
                    padding: const EdgeInsets.only(left: 72, top: 20),
                    child: Wrap(
                      spacing: 0.0,
                      runSpacing: 0.0,
                      direction: Axis.horizontal,
                      children: <Widget>[
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: gewitter()),
                        Padding(
                            padding: const EdgeInsets.all(8.0), child: regen()),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: safari()),
                        Padding(
                            padding: const EdgeInsets.all(8.0), child: wind()),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: wasserFall()),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: dschungel()),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          new Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 50.0),
                child: localAsset(),
              ),
              /* new Expanded(
                child: new Text('BBBB'),
              ),
              new Expanded(
                child: new Text('BBBB'),
              ),*/
            ],
          ),
        ],
      ),
    );
  }
}

Widget gewitter() {
  return InkWell(
    onTap: () {},
    child: Column(
      children: [
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                blurRadius: 8.0,
                spreadRadius: 0.0,
                offset: Offset(1.0, 1.0), // shadow direction: bottom right
              )
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Center(
              child: Image.asset(
                'assets/images/gewitter.png',
                height: 120,
                width: 120,
              ),
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Center(
          child: Text(
            'Gewitter',
            textAlign: TextAlign.center,
            style: new TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              fontFamily: 'SourceSerifPro',
              letterSpacing: 2.0,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget regen() {
  return InkWell(
    onTap: () {},
    child: Column(
      children: [
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                blurRadius: 8.0,
                spreadRadius: 0.0,
                offset: Offset(1.0, 1.0), // shadow direction: bottom right
              )
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Center(
              child: Image.asset(
                'assets/images/regen.png',
                height: 120,
                width: 120,
              ),
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Center(
          child: Text(
            'Regen',
            textAlign: TextAlign.center,
            style: new TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              fontFamily: 'SourceSerifPro',
              letterSpacing: 2.0,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget safari() {
  return InkWell(
    onTap: () {},
    child: Column(
      children: [
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                blurRadius: 8.0,
                spreadRadius: 0.0,
                offset: Offset(1.0, 1.0), // shadow direction: bottom right
              )
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Center(
              child: Image.asset(
                'assets/images/AfrikaSafari.png',
                height: 120,
                width: 120,
              ),
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Center(
          child: Text(
            'Safari',
            textAlign: TextAlign.center,
            style: new TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              fontFamily: 'SourceSerifPro',
              letterSpacing: 2.0,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget wind() {
  return InkWell(
    onTap: () {},
    child: Column(
      children: [
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                blurRadius: 8.0,
                spreadRadius: 0.0,
                offset: Offset(1.0, 1.0), // shadow direction: bottom right
              )
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Center(
              child: Image.asset(
                'assets/images/windLuft.png',
                height: 120,
                width: 120,
              ),
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Center(
          child: Text(
            'Wind',
            textAlign: TextAlign.center,
            style: new TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              fontFamily: 'SourceSerifPro',
              letterSpacing: 2.0,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget wasserFall() {
  return InkWell(
    onTap: () {},
    child: Column(
      children: [
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                blurRadius: 8.0,
                spreadRadius: 0.0,
                offset: Offset(1.0, 1.0), // shadow direction: bottom right
              )
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Center(
              child: Image.asset(
                'assets/images/wasserfall.png',
                height: 120,
                width: 120,
              ),
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Center(
          child: Text(
            'Wasserfall',
            textAlign: TextAlign.center,
            style: new TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              fontFamily: 'SourceSerifPro',
              letterSpacing: 2.0,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget dschungel() {
  return InkWell(
    onTap: () {},
    child: Column(
      children: [
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                blurRadius: 8.0,
                spreadRadius: 0.0,
                offset: Offset(1.0, 1.0), // shadow direction: bottom right
              )
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Center(
              child: Image.asset(
                'assets/images/dschungel.png',
                height: 120,
                width: 120,
              ),
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Center(
          child: Text(
            'Wald',
            textAlign: TextAlign.center,
            style: new TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              fontFamily: 'SourceSerifPro',
              letterSpacing: 2.0,
            ),
          ),
        ),
      ],
    ),
  );
}

/*
  Duration _duration = Duration();
  Duration _position = Duration();
  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();
    initPlayer();
  }

  void initPlayer() {
    advancedPlayer = AudioPlayer();
    audioCache = AudioCache(fixedPlayer: advancedPlayer);

    advancedPlayer.durationHandler = (d) {
      setState(() {
        _duration = d;
      });
    };

    advancedPlayer.positionHandler = (p) {
      setState(() {
        _position = p;
      });
    };
  }

  String localFilePath;

  Widget _tab(List<Widget> children) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children:
                children.map((w) => Container(child: w, padding: EdgeInsets.all(6.0))).toList(),
          ),
        ),
      ],
    );
  }

  Widget _btn(String txt, VoidCallback onPressed) {
    return ButtonTheme(
      minWidth: 48.0,
      child: Container(
        width: 150,
        height: 45,
        child: RaisedButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            child: Text(txt),
            color: Colors.pink[900],
            textColor: Colors.white,
            onPressed: onPressed),
      ),
    );
  }

  Widget slider() {
    return Slider(
        activeColor: Colors.black,
        inactiveColor: Colors.pink,
        value: _position.inSeconds.toDouble(),
        min: 0.0,
        max: _duration.inSeconds.toDouble(),
        onChanged: (double value) {
          setState(() {
            seekToSecond(value.toInt());
            value = value;
          });
        });
  }

  Widget localAsset() {
    return _tab([
      Text('Play Local Asset \'drivebymusik.mp3\':'),
      _btn('Play', () => audioCache.play('drivebymusik.mp3')),
      _btn('Pause', () => advancedPlayer.pause()),
      _btn('Stop', () => advancedPlayer.stop()),
      slider()
    ]);
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);

    advancedPlayer.seek(newDuration);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 1.0,
          backgroundColor: Colors.teal,
          title: Center(child: Text('LOCAL AUDIO')),
        ),
        body: TabBarView(
          children: [localAsset()],
        ),
      ),
    );
  }
}
*/

/*
class Music extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.cyan[100],
      child: new Center(
        child: Text('Index 3: '+AppLocalization.of(context).music),
      ),
    );
  }
}*/
