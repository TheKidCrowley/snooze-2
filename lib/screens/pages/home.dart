/// @Autor Cyrilluis

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:Snooze/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: [
        Container(
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    TypewriterAnimatedTextKit(
                        onTap: () {
                          print("Tap Event");
                        },
                        text: [
                          'Hallo,\nLass Uns Deine Reise Starten',
                        ],
                        textStyle: TextStyle(
                          fontFamily: 'SourceSerifPro',
                          fontSize: 30,
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.start,
                        alignment: AlignmentDirectional.topStart),
                    /*Text(
                      'Hallo,\nlass uns deine Reise starten',
                      style: TextStyle(
                        fontFamily: 'SourceSerifPro',
                        fontSize: 30,
                        fontWeight: FontWeight.w700,
                      ),
                    ),*/
                  ],
                ),
              )
            ],
          ),
        ),
        Center(
          child: new Image.asset(
            'assets/images/snoozeBIG.png',
            height: 400,
            width: 400,
          ),
        ),
      ],
    );
  }
}

