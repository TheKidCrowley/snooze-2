///
/// Created by Jonatan on 12.05.2020.
///

import 'dart:async';
import 'package:Snooze/bloc/langChanger.dart';
import 'package:Snooze/bloc/themeChanger.dart';
import 'package:Snooze/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'pages/import_all.dart';
import 'drawer/sidemenu.dart';
import 'package:Snooze/persistentStorage/Favorite.dart';
import 'package:Snooze/persistentStorage/sharedPref.dart';
import 'package:Snooze/persistentStorage/userdata.dart' as userdata;

class Terminal extends StatefulWidget {
  const Terminal({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TerminalState createState() => _TerminalState();
}

class _TerminalState extends State<Terminal> {
  int _pageIndex = 0;
  int _destinationDistance = 0;

  PageController _pageController = PageController(initialPage: 0);

  //pagecontents
  static List<Widget> _pageOptions = <Widget>[Home(), Aura(), Seat(), Music()];

  //style of bottomAppBar
  TextStyle _bottomAppBarStyle = const TextStyle(
    fontWeight: FontWeight.w300,
    fontSize: 15.0,
  );
  double _bottomAppBarButtonSize = 35.0;
  double _bottomAppBarButtonSizeSelected = 55.0;

  //countdown for end
  Timer _timer;
  int _remainingTime;
  DateTime _startTime;
  DateTime _endTime;
  int _duration;

  Widget _drawer;

  SharedPref sharedPref = new SharedPref();

  ThemeChanger _themeChanger;

  LanguageChanger _languageChanger;


//TODO: initialisieren der ganzen app: #sharedpreferences mit #api get call fuellen
  @override
  initState(){
    super.initState();
    
    //TODO: #api get request mit verbleibender zeit
    _duration = 30;

    _timer = Timer.periodic(Duration(seconds: 1), (_timer) => _updateTime());
    _startTime = DateTime.now();
    _endTime = _startTime.add(new Duration(minutes: _duration));
    _drawer = new SideMenuDrawer(endTime: _endTime,);
    _initSharedPreferences();
  }

  //put all retrieved values from the api into the sharedPreferences
  _initSharedPreferences() async {

    double bedLegAngle = 87.0;
    double bedBackAngle = 32.0;
    double bedMidAngle = 5.0;
    //...

    SharedPreferences prefs = await SharedPreferences.getInstance();
    //TODO: alle #api werte in #sharedPreferences abspeichern
    
    // prefs.setDouble(userdata.backRestValue, bedBackAngle);
    // prefs.setDouble(userdata.middleRestValue, bedMidAngle);
    // prefs.setDouble(userdata.feetRestValue, bedLegAngle);
    // prefs.setString(userdata.selectedColor, '12abcd');
    // prefs.setDouble(userdata.lightLevel, 9.0);
    // prefs.setDouble(userdata.volumeMusic, 3.0);
    
    // //favorites
    // sharedPref.save(userdata.favoriteOne, new Favorite('Lay', 0.0, 0.0, 90.0));
    // sharedPref.save(userdata.favoriteTwo, new Favorite('Sit', 88.0, 0.0, 0.0));
    // sharedPref.save(userdata.favoriteThree, new Favorite('Zerogravity', 45.0, 30.0, 15.0));
    // sharedPref.save(userdata.favoriteFour, new Favorite('Custom 1', 32.0, 23.0, 57.0));
    // print('terminal._init(): initialized sharedPreferences');

    //load stored language preference
    String language = prefs.getString(userdata.language);
    if(language!=null && language.isNotEmpty){
      _languageChanger.setLanguage(language);
    }
    String themeString = prefs.getString(userdata.theme);
    if(themeString!=null && themeString.isNotEmpty){
      _themeChanger.setTheme(themeString);
    }
    _rebuild();
  }

  _notifyAPI() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double bedLegAngle = prefs.get(userdata.backRestValue);
    double lightLevel = prefs.get(userdata.lightLevel);
    //...

    //TODO: #api post request mit aktuellen werten
    // api.post(bedLegAngle)
    // api.post(lightLevel)
    //...
  }

  _updateTime(){
    _remainingTime = _endTime.difference(DateTime.now()).inMinutes;
    // print('restTime: $_remainingTime');
    if(_remainingTime<3){
      _timer?.cancel();
      _informUserAboutEnding(context);
    }
  }
  Future<String> _informUserAboutEnding(BuildContext context){
    //TODO: #sharedpreferences anpassen und #api post update (licht hochfahren)
    _notifyAPI();

    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text(AppLocalization.of(context).sessionEnd),
        content: new Container(
          child: new Text(AppLocalization.of(context).thankYou),
        ),
        actions: <Widget> [
          OutlineButton(
            child: Text(AppLocalization.of(context).ok),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      );
    });
  }

  //stops the chair movement when emergency button is clicked
  _stopPress() {
    setState(() {
      print("STOP ALL IMMEDIATELYX!!1");
      //TODO: STOP ALL  notify #api
    });
  }

  //navigates to requested page
  _bottomAppBarButtonPressed(int index) {
    setState(() {
      _pageController.animateToPage(
        index,
        duration: Duration(milliseconds: 250),
        curve: Curves.linear,
      );
    });
  }

  _rebuild() => setState(() {});

  int _distAmount(int start, int end) {
    if (start - end >= 0) {
      return start - end;
    }
    return end - start;
  }

  @override
  Widget build(BuildContext context) {
    _themeChanger = Provider.of<ThemeChanger>(context);
    _languageChanger = Provider.of<LanguageChanger>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stack(
        alignment: AlignmentDirectional.bottomEnd,
        children: <Widget>[
          PageView(
            controller: _pageController,
            onPageChanged: (int) {
              _pageIndex = int;
              if (_destinationDistance > 1) {
                _destinationDistance--;
              } else {
                _rebuild();
              }
            },
            children: _pageOptions,
            scrollDirection: Axis.horizontal,
            pageSnapping: true,
            physics: BouncingScrollPhysics(),
          ),
          
          //TODO: (DONE) animateTo
          _pageIndex <= 2
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        AnimatedCrossFade(
                          alignment: Alignment.centerRight,
                          crossFadeState: _pageIndex == 0
                              ? CrossFadeState.showFirst
                              : CrossFadeState.showSecond,
                          duration: const Duration(milliseconds: 300),
                          firstCurve: Curves.decelerate,
                          secondCurve: Curves.decelerate,
                          sizeCurve: Curves.decelerate,
                          firstChild: new RaisedButton(
                            color: Theme.of(context).buttonColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                            child: new Text(
                              AppLocalization.of(context).startSnoozing,
                              style: TextStyle(
                                fontSize: 30.0,
                              ),
                            ),
                            onPressed: () => _bottomAppBarButtonPressed(
                                _pageIndex + 1 <= 3 ? _pageIndex + 1 : 3),
                            padding:
                                EdgeInsets.fromLTRB(120.0, 10.0, 120.0, 10.0),
                          ),
                          secondChild: new RaisedButton(
                            color: Theme.of(context).buttonColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            child: new Text(
                              AppLocalization.of(context).continueButton + ' >',
                              style: TextStyle(
                                fontSize: 30.0,
                              ),
                            ),
                            onPressed: () => _bottomAppBarButtonPressed(
                                _pageIndex + 1 <= 3 ? _pageIndex + 1 : 3),
                            padding: EdgeInsets.all(10.0),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                )
              : new Container(),
        ],
      ),

      endDrawer: _drawer,
      //STOP button
      floatingActionButton: Container(
        width: 120.0,
        height: 120.0,
        margin: EdgeInsets.all(6.0),
        child: ClipPath(
          clipper: _CustomClipper(),
          child: Container(
            color: Colors.white,
            child: CustomPaint(
              size: Size(120.0, 120.0),
              painter: ClipperBorderPainter(),
              child: Center(
                child: Container(
                  width: 115.0,
                  height: 115.0,
                  child: ClipPath(
                    clipper: new _CustomClipper(),
                    child: RawMaterialButton(
                      fillColor: Colors.red[600], //theme nicht beachten
                      child: const Text(
                        'STOP',
                        textScaleFactor: 2.75,
                        style: const TextStyle(color: Colors.white ,fontWeight: FontWeight.w900),
                      ),
                      onPressed: _stopPress,
                    ),
                  )
                ),
              ),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        clipBehavior: Clip.antiAlias,
        notchMargin: 6.0,
        color: Theme.of(context).bottomAppBarColor, //TODO: (DONE) theme einbauen
        child: Container(
          height: 80,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                children: _pageIndex == 0
                    ? <Widget>[
                        IconButton(
                          iconSize: _bottomAppBarButtonSizeSelected,
                          icon: Icon(Icons.home),
                          color: Theme.of(context).accentColor, //TODO: theme einbauen
                          onPressed: () {},
                        ),
                      ]
                    : <Widget>[
                        IconButton(
                          iconSize: _bottomAppBarButtonSize,
                          icon: Icon(Icons.home),
                          color: _pageIndex == 0
                              ? Theme.of(context).accentColor
                              : Theme.of(context).unselectedWidgetColor,
                          onPressed: () {
                            _destinationDistance = _distAmount(0, _pageIndex);
                            _bottomAppBarButtonPressed(0);
                          },
                        ),
                        Text(
                          AppLocalization.of(context).home,
                          style: _bottomAppBarStyle,
                        )
                      ],
              ),
              Column(
                children: _pageIndex == 1
                    ? <Widget>[
                        IconButton(
                          iconSize: _bottomAppBarButtonSizeSelected,
                          icon: Icon(Icons.lightbulb_outline),
                          color: Theme.of(context).accentColor,
                          onPressed: () {},
                        ),
                      ]
                    : <Widget>[
                        IconButton(
                          iconSize: _bottomAppBarButtonSize,
                          icon: Icon(Icons.lightbulb_outline),
                          color: _pageIndex == 1
                              ? Theme.of(context).accentColor
                              : Theme.of(context).unselectedWidgetColor,
                          onPressed: () {
                            _destinationDistance = _distAmount(1, _pageIndex);
                            _bottomAppBarButtonPressed(1);
                          },
                        ),
                        Text(
                          AppLocalization.of(context).aura,
                          style: _bottomAppBarStyle,
                        )
                      ],
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 50.0),
              ),
              Column(
                children: _pageIndex == 2
                    ? <Widget>[
                        IconButton(
                          iconSize: _bottomAppBarButtonSizeSelected,
                          icon: Icon(Icons.airline_seat_recline_normal),
                          color: Theme.of(context).accentColor,
                          onPressed: () {},
                        ),
                      ]
                    : <Widget>[
                        IconButton(
                          iconSize: _bottomAppBarButtonSize,
                          icon: Icon(Icons.airline_seat_recline_normal),
                          color: _pageIndex == 2
                              ? Theme.of(context).accentColor
                              : Theme.of(context).unselectedWidgetColor,
                          onPressed: () {
                            _destinationDistance = _distAmount(2, _pageIndex);
                            _bottomAppBarButtonPressed(2);
                          },
                        ),
                        Text(
                          AppLocalization.of(context).seat,
                          style: _bottomAppBarStyle,
                        )
                      ],
              ),
              Column(
                children: _pageIndex == 3
                    ? <Widget>[
                        IconButton(
                          iconSize: _bottomAppBarButtonSizeSelected,
                          icon: Icon(Icons.music_note),
                          color: Theme.of(context).accentColor,
                          onPressed: () {},
                        ),
                      ]
                    : <Widget>[
                        IconButton(
                          iconSize: _bottomAppBarButtonSize,
                          icon: Icon(Icons.music_note),
                          color: _pageIndex == 3
                              ? Theme.of(context).accentColor
                              : Theme.of(context).unselectedWidgetColor,
                          onPressed: () {
                            _destinationDistance = _distAmount(3, _pageIndex);
                            _bottomAppBarButtonPressed(3);
                          },
                        ),
                        Text(
                          AppLocalization.of(context).music,
                          style: _bottomAppBarStyle,
                        )
                      ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _CustomClipper extends CustomClipper<Path> {
  //clip stop button shape
  @override
  Path getClip(Size size) {
    final path = Path();
    final double _width = size.width;
    final double _height = size.height;

    final double closeMultiplicator = .3;
    final double farMultiplicator = 1-closeMultiplicator;

    path.moveTo(0.0, _height*closeMultiplicator);
    path.lineTo(0.0, _height*farMultiplicator);
    path.lineTo(_width*closeMultiplicator, _height);
    path.lineTo(_width*farMultiplicator, _height);
    path.lineTo(_width, _height*farMultiplicator);
    path.lineTo(_width, _height*closeMultiplicator);
    path.lineTo(_width*farMultiplicator, 0.0);
    path.lineTo(_width*closeMultiplicator, 0.0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class ClipperBorderPainter extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {
    var _borderPaint = Paint()
      ..color = Colors.black
      ..strokeWidth = 2.0
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    var path = Path();
    final double _width = size.width;
    final double _height = size.height;

    final double closeMultiplicator = .3;
    final double farMultiplicator = 1-closeMultiplicator;

    path.moveTo(0.0, _height*closeMultiplicator);
    path.lineTo(0.0, _height*farMultiplicator);
    path.lineTo(_width*closeMultiplicator, _height);
    path.lineTo(_width*farMultiplicator, _height);
    path.lineTo(_width, _height*farMultiplicator);
    path.lineTo(_width, _height*closeMultiplicator);
    path.lineTo(_width*farMultiplicator, 0.0);
    path.lineTo(_width*closeMultiplicator, 0.0);

    path.close();

    canvas.drawPath(path, _borderPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

}