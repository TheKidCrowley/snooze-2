///
/// Created by Jonatan on 21.05.2020.
///
import 'package:Snooze/bloc/langChanger.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:Snooze/persistentStorage/userdata.dart' as userdata;

enum Language {de, en, es}

class SettingsLangPage extends StatefulWidget {
  SettingsLangPage({Key key}) : super(key: key);

  @override
  _SettingsLangPageState createState() => _SettingsLangPageState();
}

class _SettingsLangPageState extends State<SettingsLangPage> {

  Language _language = Language.en;

  LanguageChanger _languageChanger;

  @override
  initState(){
    super.initState();
    _loadLanguage();
  }
  
  _loadLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String lang = prefs.getString(userdata.language);
    if(lang!=null && lang.isNotEmpty){
      switch(lang){
        case userdata.languageEN: _onSelect(Language.en);
                                  return;
        case userdata.languageDE: _onSelect(Language.de);
                                  return;
        case userdata.languageES: _onSelect(Language.es);
                                  return;
      }
    }
    _onSelect(_language);
  }

  _onSelect(Language value){
    setState(() {
      this._language = value;
      _languageChanger.setLanguage(_language.toString().split('.')[1]);
      _updateLanguage();
    });
  }

  _updateLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(userdata.language, _language.toString().split('.')[1]);
  }

  @override
  Widget build(BuildContext context){
    _languageChanger = Provider.of<LanguageChanger>(context);
    return new ListView(
      children: [
        new RadioListTile <Language>(
          title: const Text('English'),
          value: Language.en,
          groupValue: _language,
          onChanged: _onSelect,
        ),
        new RadioListTile <Language>(
          title: const Text('Deutsch'),
          value: Language.de,
          groupValue: _language,
          onChanged: _onSelect,
        ),
        new RadioListTile <Language>(
          title: const Text('Español'),
          value: Language.es,
          groupValue: _language,
          onChanged: _onSelect,
        ),
      ],
    );
  }
}