///
/// Created by Jonatan on 09.06.2020.
///

import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class SharedPref {
  read(String key) async {
    final prefs = await SharedPreferences.getInstance();
    try{
      String result = prefs.getString(key);
      if(result!=null && result.isNotEmpty){
        return json.decode(result);
      }
    }
    catch(Exception){
      print('sharedPref.read() Error: could not get data from sharedPreferences');
    }
  }

  save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }
}
